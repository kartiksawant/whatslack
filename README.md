# WhatSlack

### Flowchart
![Flowchart](http://drive.google.com/uc?export=view&id=1en-yiklLblka1yJvLQ7_yFSNSmqQiwla)

### Socket API Interface
![Flowchart](http://drive.google.com/uc?export=view&id=17ieAufDzdY6B-LQiENZ6D-AKwl6JnU11)

### User Interface
#### Web
![Flowchart](http://drive.google.com/uc?export=view&id=1e0ZMaszWzo19t0qntKslyuZzA3y0bIIg)
#### Mobile
![Flowchart](http://drive.google.com/uc?export=view&id=1rIXpKBad8CZDkihAe8b3N6_zPs83yXwO)
Theme Color Code : #360D37 
## Database Schema
#### Users Collection

| Parameters | Type | Description |
| :-------:  | :--: | :---------: |
| _id  | _ObjectId_ | Document Id |
| username   | _String_ | Name    | 
| password | _String_ | Password |
| status | _boolean_ | online or offline   |
| lastseen | _timestamp_ | Time lastseen if offline |

##### Conversation Collection 

| Parameters | Type | Description |
| :--------: | :--: | :---------: |
| id | _ObjectId_ | Id of Conversation |
| date | _timestamp_ | date of last message sent |
| lastmessage | _string_ | last message |
| recipients | _Array_ | Array of users involved | 

##### transactions collection 

| Parameters | Type | Description |
| :--------: | :--: | :---------: |
| \_id | _Number_ | id of the message | 
| type | _String_ | new, edit or delete |
| from | _String_ | Sender |
| conversationId | _ObjectId_ | Id of the conversation |
| body | _Object_ | Transaction Content |

# API Description
##### List of API endpoints : 

User API's
1. POST user/login
1. POST user/register
1. GET user/status

Chat API's
1. POST chat/create
1. GET chat/conversations
1. GET chat/messages?userid
1. POST chat/transaction
1. GET chat/transaction

##### Base URL : `http://localhost:5000/routes/api/`

# User API specifications
## 1. "/user/login" - API Specification
Description : For signing in of an existing user. Checks the login credentials, validate and authenticates the user if the user is a valid user.
#### Request Method
`POST`
#### Request URL
`BASE URL + /user/login`

#### Request Body
| Parameters | Description |
|:--:|:--:|
| username  | username of the user | 
| password  | password of the user | 

#### Success Response
- Structure 

| Parameter | Type  | 
|:--:|:--:|
| statusCode  | _number_ |
| username | _string_ |
| userId | _ObjectId_ |

- Sample

```json
{
    "statusCode": 200,
    "username" : "user1",
    "userId" : "s7f8f59e55a8f5",
}
```

#### Error Response
- Sample

```json
{
    "statusCode": "400",
    "error": "Bad Request / Wrong Password"
}
```
```json
{
    "statusCode": "404",
    "error": "User Not Found"
}
```

## 2. "/user/register" -  API Specification
Descriptions : For registering a new user. Will create a new account for the user with a unique username.
#### Request Method
`POST`
#### Request URL
`BASE URL + /user/register`
#### Request Body
| Parameters | Description |
|:--:|:--:|
| username | username of the user |
| password | password of the user |

#### Success Response
- Structure 

| Parameter | Type  |
|:--:|:--:|
| statusCode  | _number_  |
| username | _string_ |

- Sample

```json
{
    "statusCode": 200,
    "username" : "User1",
}
```

#### Error Response
- Sample

```json
{
    "statusCode": "400",
    "error": "Bad Request/ User already exists"
}
```
```json
{
    "statusCode": "500",
    "error": "Internal Server Error"
}
```

## 3. "/user/status" - API Specification
Description : For showing status i.e it will show online if the user is connected and logged in and lastseen time of the user if the user is offline.
#### Request Method
`GET`
#### Request URL
`BASE URL + /user/status`
#### Request Body
| Parameters | Description |
|:--:|:--:|
| username | username of the user |


#### Success Response
- Structure 

| Parameter | Type  |
|:--:|:--:|
| statusCode  | _number_  |
| status | _boolean_ |
| lastseen | _timestamp_ |

- Sample

```json
{
    "statusCode": 200,
    "lastseen" : 74125841663,
}
```

#### Error Response
- Samples

```json
{
    "statusCode": "400",
    "error": "Bad Request"
}
```
```json
{
    "statusCode": "500",
    "error": "Internal Server Error"
}
```
```json
{
    "statusCode": "401",
    "error": "Unauthorised"
}
```


# Chat API specifications


## 1. "/chat/create" -  API Specification
Description : Create a conversation (P2P/Group) by searching and adding users.
Would accept an array of recipients and create a conversation with a initial welcome message.

#### Request Method
`POST`
#### Request URL
`BASE URL + /chat/create`

#### Request Body
- Structure 

| Parameter | Description  |
|:--:|:--:|
| usernames  | _Array_ |

#### Success Response
- Structure 

| Parameter | Type  |
|:--:|:--:|
| statusCode  | _number_  |

- Sample

```json
{
    "statusCode": 201,
}
```

#### Error Response
- Sample
```json
{
    "statusCode": "400",
    "error": "Bad Request/ Users do not exist"
}
```
```json
{
    "statusCode": "500",
    "error": "Internal Server Error"
}
```
```json
{
    "statusCode": "401",
    "error": "Unauthorised"
}
```

## 2. "/chat/conversations" -  API Specification
Description : Get all the conversations/chat list of the user with whom the user has conversed.

#### Request Method
`GET`
#### Request URL
`BASE URL + /chat/conversations`

#### Success Response
- Structure 

| Parameter | Type  |
|:--:|:--:|
| statusCode  | _number_  |
| conersations | _Array_ | 

- Sample

```json
{
    "statusCode": 200,
    "conversations" : [],
}
```

#### Error Response
- Sample
```json
{
    "statusCode": "400",
    "error": "Bad request"
}
```
```json
{
    "statusCode": "500",
    "error": "Internal Server Error"
}
```
```json
{
    "statusCode": "401",
    "error": "Unauthorised"
}
```
## 3. "/chat/messages?userid" -  API Specification
Description : To get all the messages of a particular conversation and
set all messages recieved by the user from that conversation to seen.
#### Request Method
`GET`
#### Request URL
`BASE URL + /chat/messages?userid`

#### Success Response
- Structure 

| Parameter | Type  |
|:--:|:--:|
| statusCode  | _number_  |
| message | Array |


- Sample

```json
{
    "statusCode": 200,
    "message" : [{
        "conversationId" : "a78d52d9w5s0a56",
		"from" : "user1",
	    "to" : "User2",
        "body" : "Hello World",
        "sent" : 4120589630,
        "delivered" : 4589615198,
        "seen" : 7896541230,
    }],
}
```

#### Error Response
- Sample
```json
{
    "statusCode": "400",
    "error": "Bad request"
}
```
```json
{
    "statusCode": "500",
    "error": "Internal Server Error"
}
```
```json
{
    "statusCode": "401",
    "error": "Unauthorised"
}
```

## 4. "/chat/transaction" -  API Specification
Description : To add a new transaction according to the type of transaction selected by the user.
There would be three types of transactions: new, edit or delete
#### Request Method
`POST`
#### Request URL
`BASE URL + /chat/transaction`

#### Request Body
| Parameters | Description |
|:--:|:--:|
| id | id of the reciever |
| userid | id of sender |
| type | type of transaction |
| body | body of the transaction (would contain transactionid if edit or delete) |


#### Success Response
- Structure 

| Parameter | Type  |
|:--:|:--:|
| statusCode  | _number_  |

- Sample

```json
{
    "statusCode": 200,
}
```

#### Error Response
- Sample
```json
{
    "statusCode": "400",
    "error": "Bad request"
}
```
```json
{
    "statusCode": "500",
    "error": "Internal Server Error"
}
```
```json
{
    "statusCode": "401",
    "error": "Unauthorised"
}
```

## 5. "/chat/transaction" -  API Specification
Description : To fetch the pending transaction on "message" socket event.
#### Request Method
`GET`
#### Request URL
`BASE URL + /chat/transaction`

#### Request Body
| Parameters | Description |
|:--:|:--:|
| id | id of the reciever |
| userid | id of sender |
| transid | id of the last transaction |

#### Success Response
- Structure 

| Parameter | Type  |
|:--:|:--:|
| statusCode  | _number_  |

- Sample

```json
{
    "statusCode": 200,
    "transactions": []
}
```

#### Error Response
- Sample
```json
{
    "statusCode": "400",
    "error": "Bad request"
}
```
```json
{
    "statusCode": "500",
    "error": "Internal Server Error"
}
```
```json
{
    "statusCode": "401",
    "error": "Unauthorised"
}
```

