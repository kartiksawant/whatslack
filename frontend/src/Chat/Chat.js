import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import AddUserForm from "./AddUserForm";
import ConversationList from "./ConversationList";
import Header from "./Header";
import MessageWindow from "./MessageWindow";
import socketIOClient from "socket.io-client";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    overflowY: 'hidden',
    overflowX: "hidden"
  },
}));

let socket = socketIOClient(process.env.REACT_APP_API_URL);

const Chat = () => {
  const classes = useStyles();
  const [updateList, setUpdateList] = useState(false);
  const [chatWith, setChatWith] = useState("");

  const update = () => {
    setUpdateList(!updateList);
  };

  let user = JSON.parse(localStorage.getItem("currentUser")).username;

  useEffect(() => {
    console.log('New connection');
    console.log("user connected", user);
    socket.emit("login", user);

    return () => {
      socket.emit("logout");
    }
  }, []);

  return (
    <div className={classes.root}>
      <Header socket={socket}/>
      <React.Fragment>
        <Grid container spacing={1}>
          <Grid item xs={3} style={{marginTop: 4, backgroundColor: '#3e0e40'}}>
            <div>
              <AddUserForm update={update} />
            </div>
            <ConversationList
              updateList={updateList}
              updateUser={setChatWith}
              socket={socket}
            />
          </Grid>
          <Grid id="message_box" container item xs={9}>
            {!chatWith ? (
              <div style={{textAlign: 'center', flexGrow:1, marginTop: 'auto', marginBottom: 'auto', fontSize: 30}}>Welcome to WhatSlack!</div>
            ) : (
              <MessageWindow user={chatWith} socket={socket} />
            )}
          </Grid>
        </Grid>
      </React.Fragment>
    </div>
  );
};

export default Chat;
