import React, { useState, useEffect, useRef } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import DoneIcon from '@material-ui/icons/Done';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import Paper from "@material-ui/core/Paper";
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Tooltip from "@material-ui/core/Tooltip";
import { useSnackbar } from "notistack";
import moment from 'moment';
import Zoom from '@material-ui/core/Zoom';
import Typography from '@material-ui/core/Typography';
const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 1000,
    height: "58vh",
    padding: 10,
  },
  title: {
    margin: theme.spacing(2, 0, 2),
    textAlign: "center",
  },
  secondaryAction: {
    marginRight: 40,
    top: 10,
  },
  secondaryActionDelete: {
    top: 10,
  },
  tick:{
    marginLeft: 15,
  },
  timestamp:{
    marginLeft: 15,
    paddingBottom: 25,
    fontSize: 12,
    color: "#555555"
  },
  messageBubble: {
    padding: 10,
    backgroundColor: "#ebebeb",
    borderRadius: "0 20px 20px 20px",
    boxShadow: "0px 4px 4px -3px rgba(0,0,0,0.1)",
    marginTop: 8,
    maxWidth: "30em",
  },
  messageBubbleRight: {
    padding: 10,
    backgroundColor: "#dcf8c7",
    boxShadow: "-3px 4px 4px 0px rgba(0,0,0,0.1)",
    marginTop: 8,
    marginLeft: 500,
    maxWidth: "40em",
    borderRadius: "20px 0 20px 20px",
  },
}));

export default function Messages(props) {
  const classes = useStyles();
  const [messages, setMessages] = useState([]);
  const [shouldUpdateMessages, setShouldUpdateMessages] = useState(false);
  const [user, setUser] = useState("");
  const [hashmap, setHashmap] = useState({});
  const [edited, setEdited] = useState("");
  const [editId, setEditId] = useState('');
  const { enqueueSnackbar } = useSnackbar();
  const [open, setOpen] = useState(false);
  let url;
  const socket = props.socket;
  const HtmlTooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: '#f5f5f5',
      color: 'rgba(0, 0, 0, 0.87)',
      maxWidth: 300,
      fontSize: 12,
      border: '1px solid #dadde9',
    },
  }))(Tooltip);
  const handleClickOpen = (id) => {
    setEditId(id);
    setOpen(true);
  };

  const handleClose = () => {
    setEdited("");
    setOpen(false);
  };
  const onChange = (e) => {
    console.log(e.target.value);
    setEdited(e.target.value);
  };
  const editMessage = () => {
    fetch(`${process.env.REACT_APP_API_URL}/chat/transactions`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: JSON.parse(localStorage.getItem("currentUser")).token,
      },
      body: JSON.stringify({
        type: "edit",
        conversation: props.chatWith._id,
        body: edited,
        transId: editId,
      }),
    })
    .then(data=>{
      console.log("edited");
      handleClose();
    })
    .catch(err=>{
      console.log(err);
    })
    setEdited("");
  };
  const deleteMessage = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}/chat/transactions`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: JSON.parse(localStorage.getItem("currentUser")).token,
      },
      body: JSON.stringify({
        type: "delete",
        conversation: props.chatWith._id,
        transId: id,
      }),
    })
    .then(data=>{
      console.log("deleted");
    })
    .catch(err=>{
      if (id != -86) {
        console.log(err);
      }
    })
  };

  // useEffect(() => {deleteMessage(-86)}, []) // Hack for seen status updates
  
  useEffect(() => {
    socket.on("message",(data)=>{
        setShouldUpdateMessages(shouldUpdateMessages => !shouldUpdateMessages)
        console.log("new message",shouldUpdateMessages);
        // deleteMessage(-86);
    })
}, [])
  useEffect(() => {
    if (user !== props.chatWith) {
      url = `${
        process.env.REACT_APP_API_URL
      }/chat/transactions?${new URLSearchParams({
        conversation: props.chatWith._id,
        lastId: 0,
      })}`;
    } else {
      url = `${
        process.env.REACT_APP_API_URL
      }/chat/transactions?${new URLSearchParams({
        conversation: props.chatWith._id,
        lastId: messages.length > 0 ? messages[messages.length - 1].key : 0,
      })}`;
    }
    fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: JSON.parse(localStorage.getItem("currentUser")).token,
      },
    })
      .then((data) => {
        return data.json();
      })
      .then((chatData) => {
        console.log(chatData);
        let tempObj;
        if (user !== props.chatWith) {
          setUser(props.chatWith);
          tempObj = {};
        } else {
          tempObj = { ...hashmap };
        }
        chatData.map((val) => {
          if (val.type === "new") {
            tempObj[val._id] = val;
          } else if (val.type === "edit") {
            tempObj[val.body.transaction].body = val.body;
          } else {
            delete tempObj[val.body.transaction];
          }
        });
        return tempObj;
      })
      .then((tempObj) => {
        setHashmap(tempObj);
        return tempObj;
      })
      .then((tempObj) => {
        console.log(tempObj, "tempObj");
        let tempArr = Object.values(tempObj);
        return tempArr;
      })
      .then((tempArr) => {
        console.log(tempArr, "tempArr");
        setMessages(
          tempArr.map((val) => {
            if(val.body.seen!=0){
              var tick = <DoneAllIcon style={{fontSize: 20, color: 'blue'}} />
              var tip = <React.Fragment>
              Sent: {moment(val.body.sent).format('MMM Do YYYY, h:mm a')} <br></br>
              Delivered: {moment(val.body.delivered).format('MMM Do YYYY, h:mm a')} <br></br>
              Seen: {moment(val.body.seen).format('MMM Do YYYY, h:mm a')}
            </React.Fragment>
            }else if(val.body.delivered!=0){
              var tick = <DoneAllIcon style={{fontSize: 20}} />
              var tip = <React.Fragment>
              Sent: {moment(val.body.sent).format('MMM Do YYYY, h:mm a')} <br></br>
              Delivered: {moment(val.body.delivered).format('MMM Do YYYY, h:mm a')} <br></br>
            </React.Fragment>
            }else{
              var tick = <DoneIcon style={{fontSize: 20}} />
              var tip = <React.Fragment>
              Sent: {moment(val.body.sent).format('MMM Do YYYY, h:mm a')} <br></br>
            </React.Fragment>
            }
            return (
              <Paper
                elevation={3}
                key={val._id}
                className={
                  val.from ===
                  JSON.parse(localStorage.getItem("currentUser")).username
                    ? classes.messageBubbleRight
                    : classes.messageBubble
                }
              >
                <ListItem>
                  <ListItemText 
                  primary={<React.Fragment><Typography style={{fontWeight: "700", color:'#075E54'}}>{val.from}</Typography></React.Fragment>} 
                  secondary={<React.Fragment><Typography style={{fontWeight: "400", fontSize: 15, color:'black'}}>{val.body.body}</Typography></React.Fragment>} />
                  {val.from ===
                    JSON.parse(localStorage.getItem("currentUser"))
                      .username && (
                    <ListItemSecondaryAction
                      className={classes.secondaryAction}
                    >
                      <IconButton
                        size="small"
                        edge="end"
                        aria-label="edit"
                        onClick={() => handleClickOpen(val._id)}
                      >
                        <EditIcon style={{ fontSize: 20 }} />
                      </IconButton>
                    </ListItemSecondaryAction>
                  )}
                  {val.from ===
                    JSON.parse(localStorage.getItem("currentUser"))
                      .username && (
                    <ListItemSecondaryAction
                      className={classes.secondaryActionDelete}
                    >
                      <IconButton
                        size="small"
                        edge="end"
                        aria-label="delete"
                        onClick={() => deleteMessage(val._id)}
                      >
                        <DeleteIcon style={{ fontSize: 20 }} />
                      </IconButton>
                    </ListItemSecondaryAction>
                  )}
                </ListItem>
                {val.from !=
                  JSON.parse(localStorage.getItem("currentUser")).username && (
                  <span className={classes.timestamp}>
                    {moment(val.body.sent).format('MMM Do YYYY, h:mm a')}
                  </span>)
                }
                {val.from ===
                  JSON.parse(localStorage.getItem("currentUser")).username && (
                  <HtmlTooltip title={tip} TransitionComponent={Zoom}>
                    <ListItemIcon className={classes.tick}>{tick}</ListItemIcon>
                  </HtmlTooltip>
                )}
              </Paper>
            );
          })
        );
      })
      .then(() => {
        console.log(messages, "Messages");
      })
      .catch((err) => {
        console.log(err);
        enqueueSnackbar("Failed to fetch transactions", {
          variant: "error",
        });
      });
  }, [props.chatWith, shouldUpdateMessages]);
  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };
  
  useEffect(scrollToBottom, [messages]);
  return (
    <div className={classes.root}>
    <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" maxWidth="sm" fullWidth={true}>
        <DialogTitle id="form-dialog-title">Edit message</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="edited"
            label="New message"
            type="text"
            fullWidth={true}
            onChange={onChange}
            
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={editMessage} color="primary">
            Edit
          </Button>
        </DialogActions>
      </Dialog>
      <List dense={true}>{messages}</List>

      <div ref={messagesEndRef} />
    </div>
  );
}