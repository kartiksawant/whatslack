import React, { useState } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import AddIcon from "@material-ui/icons/Add";
import { useSnackbar } from "notistack";
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  sidebar: {
    zIndex: 8,
    border: "2px solid black",
  },
  margin: {
    margin: theme.spacing(1),
  },
  form: {
    display: "flex",
    marginBottom: 0,
    color: '#eeeeee',
  },
  button: {
    color: '#eeeeee',
    marginLeft: 10,
  },
  title: {
    marginLeft: 'auto',
    marginRight: 'auto',
    textAlign: "center",
    marginBottom: 10,
    color: '#eeeeee'
  },
  multilineColor:{
    color:'#eeeeee'
}
}));
const CssTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: '#eeeeee',
    },
    '& label' : {
      color: '#eeeeee',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#eeeeee',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#eeeeee',
      },
      '&:hover fieldset': {
        borderColor: '#ffffff',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#eeeeee',
      },
    },
  },
})(TextField);

const AddUserForm = (props) => {
  const classes = useStyles();
  const [userList, setUserList] = useState("");
  const { enqueueSnackbar } = useSnackbar();

  const onChange = (e) => {
    setUserList(e.target.value);
  };

  const onClick = (e) => {
    if (!userList) {
      enqueueSnackbar("Please enter the username", {
        variant: "warning",
      });
      return null;
    }
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: JSON.parse(localStorage.getItem("currentUser")).token,
      },
      body: JSON.stringify({ members: userList }),
    };
    console.log(requestOptions);
    fetch(`${process.env.REACT_APP_API_URL}/chat/create`, requestOptions)
      .then((res) => {
        if (res.status === 400) {
          enqueueSnackbar("Username does not exist", {
            variant: "warning",
          });
        } else {
          document.getElementById("outlined-basic").value = "";
          props.update();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className={classes.root}>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex">
          <Grid item className={classes.form}>
            <CssTextField
              InputProps={{
                className: classes.multilineColor
              }}
              color= '#eeeeee'
              id="mui-theme-provider-standard-input"
              label="Create conversation"
              variant="outlined"
              style = {{width: 250}}
              onChange={onChange}
              fullWidth= {true}
            />
            <IconButton
              aria-label="add"
              className={classes.margin}
              onClick={onClick}
              color="#eeeeee"
            >
              <AddIcon style={{color: '#eeeeee'}} />
            </IconButton>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default AddUserForm;
