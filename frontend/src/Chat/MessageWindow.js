import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ChatHeader from "./ChatHeader";
import Messages from "./Messages";
import WriteMessage from "./WriteMessage";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 1100,
    marginLeft: -5
  },
  conversations: {
    backgroundColor: theme.palette.background.paper,
    marginBottom: "20px",
  },
  title: {
    margin: theme.spacing(2, 0, 2),
    textAlign: "center",
  },
}));

export default function MessageWindow(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <ChatHeader chatWith={props.user} socket={props.socket} />
        </Grid>
        <Grid
          style={{
            maxHeight: window.innerHeight - 80,
            overflowY: "auto",
            overflowX: "hidden",
          }}
          item
          xs={12}
        >
          <Messages chatWith={props.user} socket={props.socket}/>
        </Grid>
        <Grid item xs={12}>
          <WriteMessage socket={props.socket} chatWith={props.user} />
        </Grid>
      </Grid>
    </div>
  );
}
