import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";
import PeopleIcon from '@material-ui/icons/People';
import Paper from "@material-ui/core/Paper";
import { useSnackbar } from "notistack";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  conversations: {
    backgroundColor: theme.palette.background.paper,
    marginBottom: "20px",
  },
  title: {
    margin: theme.spacing(2, 0, 2),
    textAlign: "center",
  },
}));

export default function ConversationList(props) {
  const classes = useStyles();
  const [conversations, setConversations] = useState([]);
  const [shouldUpdateConversations, setShouldUpdateConversations] = useState(false);
  const { enqueueSnackbar } = useSnackbar();
  const socket = props.socket;
  useEffect(() => {
    socket.on("message",(data)=>{
      setShouldUpdateConversations(shouldUpdateConversations => !shouldUpdateConversations)
        console.log("new message",shouldUpdateConversations);
    })
}, [])
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/chat/conversations`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: JSON.parse(localStorage.getItem("currentUser")).token,
      },
    })
      .then((res) => {
        const data = res.json();
        return data;
      })
      .then((data) => {
        console.log(data);
        setConversations(
          data.map((val) => {
            return (
              <Paper
                elevation={3}
                key={Math.floor(Math.random() * 100000)}
                onClick={() => props.updateUser(val)}
                style={{ cursor: "pointer", marginBottom: 10, boxShadow: "0px 4px 4px -3px rgba(0,0,0,0.1)", backgroundColor: '#3e0e40' }}
              >
                <ListItem>
                  <ListItemAvatar>
                    <Avatar style={{height: 30, width: 30}}>
                      <PeopleIcon style={{color: "#3e0e40"}}/>
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                  style={{color: '#eeeeee', fontWeight: '200'}}
                    primary={val.recipients
                      .filter(
                        (val) =>
                          JSON.parse(localStorage.getItem("currentUser"))
                            .username !== val
                      )
                      .join(", ")}
                  />
                </ListItem>
              </Paper>
            );
          })
        );
      })
      .catch((err) => {
        console.log(err);
        enqueueSnackbar("Failed to fetch conversations", {
          variant: "error",
        });
      });
  }, [props.updateList,shouldUpdateConversations]);

  return (
    <div className={classes.root}>
      <Grid container spacing={2} style={{height: '80vh', overflowY: 'auto', overflowX: 'hidden'}}>
        <Grid item xs={12}>
          <div className={classes.conversations}>
            <List dense={false} style={{backgroundColor: '#3e0e40'}}>{conversations}</List>
          </div>
        </Grid>
      </Grid>
    </div>
  );
}
