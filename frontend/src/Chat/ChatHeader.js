import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 1200,
    padding: 10,
    fontSize: 18,
    borderBottom: "1px solid #ebebeb",
    backgroundColor: '#ebebeb',
    marginLeft: 1
  },
  conversations: {
    backgroundColor: theme.palette.background.paper,
    marginBottom: "20px",
  },
  title: {
    margin: theme.spacing(2, 0, 2),
    textAlign: "center",
  },
}));

export default function ChatHeader(props) {
    const classes = useStyles();
    const [status, setStatus] = useState(false)
    const [lastSeen, setLastSeen] = useState('');
    const [shouldUpdateStatus, setShouldUpdateStatus] = useState(false);
    const socket = props.socket;
    useEffect(() => {
        socket.on("online",(data)=>{
            setShouldUpdateStatus(shouldUpdateStatus => !shouldUpdateStatus)
            console.log("staus changed",shouldUpdateStatus);
        })
        socket.on("offline",(data)=>{
            setShouldUpdateStatus(shouldUpdateStatus => !shouldUpdateStatus)
            console.log("staus changed",shouldUpdateStatus);
        })
        // socket.on('offline', (data) => {
        //     if (props.chatWith.recipients.filter((val) => val !== JSON.parse(localStorage.getItem('currentUser')).username).join(', ') == data) {
        //         setStatus(false);
        //     }
        // })
    }, [])
    useEffect(() => { console.log(props )}, [props])
    useEffect(() => {
        console.log("useeffect called")
        let url = `${
            process.env.REACT_APP_API_URL
          }/user/status?${new URLSearchParams({
            name: props.chatWith.recipients.filter((val)=>val!==JSON.parse(localStorage.getItem('currentUser')).username)[0],
          })}`;
          fetch(url, {
            method: "GET",
            headers: {
              "Content-Type": "application/json",
              Authorization: JSON.parse(localStorage.getItem("currentUser"))
                .token,
            },
          })
          .then(data=>{
              return data.json();
              
          })
          .then(result=>{
              console.log(result[0]);
            setStatus(result[0].status);
            setLastSeen(result[0].timestamp);
          })
          .catch(err=>{
              console.log(err);
          })
    }, [props.chatWith.recipients,shouldUpdateStatus])
    return (
        <div className={classes.root}>
            {props.chatWith ? props.chatWith.recipients.filter((val)=>val!==JSON.parse(localStorage.getItem('currentUser')).username).join(', ') : 'Hello :)'}
            <br></br>{props.chatWith.recipients.length==2 ? (<div style={{fontSize: 12}}>{status ? "Online": ("Last Seen " + moment(lastSeen).add(24, 'hours').format('LLL'))}</div>):(<div style={{fontSize: 12, color:'#ebebeb'}}>......</div>)}
        </div>
    );
}
