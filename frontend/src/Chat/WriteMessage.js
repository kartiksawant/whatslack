import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import SendIcon from "@material-ui/icons/Send";
import TextField from "@material-ui/core/TextField";
import { useSnackbar } from "notistack";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: "flex",
    maxWidth: 1000,
  },
}));
let to, from;
export default function WriteMessage(props) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const [message, setMessage] = useState("");
  const [typing, setTyping] = useState(false);
  const [user, setUser] = useState("");
  const [show, setDisplay] = useState("");

  const socket = props.socket;
  if (props.chatWith.recipients.length == 2) {
    to = props.chatWith.recipients.filter(
      (val) => val !== JSON.parse(localStorage.getItem("currentUser")).username
    )[0];
  }
  from = JSON.parse(localStorage.getItem("currentUser")).username;

  const onChange = (e) => {
    console.log(e.target.value);
    setMessage(e.target.value);
  };

  // useEffect(() => {
  //   socket.on("display", (data) => {
  //     setTyping(data.status);
  //     setUser(data.from);
  //     console.log("typing...");
  //     console.log(typing, data.from, to, "check it");
  //     if (!typing && data.from === to && props.chatWith.recipients.length == 2)
  //       setDisplay("typing...");
  //     else setDisplay("");
  //   });
  // }, [typing]);
  useEffect(() => {
    socket.on("display", (data) => {
      setTyping(data.status);
      setUser(data.from);
      console.log("typing...");
      console.log(typing, data.from, to, "check it");
      if (!typing && data.from === to && props.chatWith.recipients.length == 2) {
        setDisplay("typing...");
        setTimeout(() => setDisplay(''), 1000);
      }
      else setDisplay("");
    });
  }, []);

  const emitTyping = () => {
    console.log(socket.id, to, from);
    socket.emit("typing", { to: to, from: from });
  };

  // const typingEnd = () => {
    // socket.emit("typingend", { to: to, from: from });
  // };

  const onClick = () => {
    console.log(props);
    fetch(`${process.env.REACT_APP_API_URL}/chat/transactions`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: JSON.parse(localStorage.getItem("currentUser")).token,
      },
      body: JSON.stringify({
        type: "new",
        body: message,
        conversation: props.chatWith._id,
      }),
    })
      .then((res) => {
        console.log(res);
        setMessage("");
      })
      .catch((err) => {
        console.log(err);
        enqueueSnackbar("Failed to send message", {
          variant: "error",
        });
      });
  };

  return (
    <div>
      <div>
      {show.length===0 ? (<p style={{ marginLeft: 20,color:'white' }}>........</p>):
      <p style={{ marginLeft: 20 }}>{show}</p>}
        {console.log(show, "show")}
      </div>
      <div></div>
      <div className={classes.root}>
        <TextField
          id="outlined-basic"
          label="Send Message"
          variant="outlined"
          onChange={onChange}
          onKeyDown={() => emitTyping()}
          // onKeyUp={() => typingEnd()}
          style={{ margin: 5 }}
          value={message}
          placeholder="Type your message..."
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <IconButton
          onClick={onClick}
          aria-label="delete"
        >
          <SendIcon />
        </IconButton>
      </div>
    </div>
  );
}
