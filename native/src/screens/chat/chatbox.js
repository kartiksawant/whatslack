import * as React from 'react';
import {useState, useEffect, useRef} from 'react';
import ActionSheet from 'react-native-action-sheet';
import styles from './styles';
import axios from 'axios';
import moment from 'moment';
import {
  View,
  Text,
  FlatList,
  SafeAreaView,
  TouchableWithoutFeedback,
  Alert,
  KeyboardAvoidingView,
  TextInput,
  Modal,
  TouchableHighlight,
  ActivityIndicator,
} from 'react-native';
import {Icon, Divider} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import { Dimensions } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LogBox } from 'react-native';
LogBox.ignoreLogs(['Warning: ...', 'Setting', 'VirtualizedLists should ', 'Non-serializable', 'Encountered']); // Ignore log notification by message

window.navigator.userAgent = 'react-native';
import socketIO from 'socket.io-client';

const width = Dimensions.get('window').width; //full width


const ChatBox = (props) => {
  const [created, setcreated] = useState([]);
  const [msg, setMsg] = useState('');
  const [edited, setEdit] = useState('');
  const [editConvo, setEditConvo] = useState('');
  const [editId, setEditId] = useState('');
  const [trans, setTrans] = useState([]);
  const [lastId, setLastId] = useState(-1);
  const [map, setMap] = useState(new Map());
  const [online, setOnline] = useState(false);
  const [isTyping, setIsTyping] = useState(false);
  const [whoIsTyping, setWhoIsTyping] = useState('');
  const [isGroup, setIsGroup] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [user, setUser] = React.useState('');
  const [self, setSelf] = useState('');
  const socket = useRef();
  const scrollViewRef = useRef(); 

  let a = -1;

  const createmsgs_legacy = () => {
    trans.forEach((transs) => {
      if (transs.type == 'new') {
        map.set(transs._id.toString(), transs);
      } else if (transs.type == 'edit') {
        let temp = map.get(transs.body.transaction);
        if (temp != undefined) {
          temp.body.body = transs.body.body;
          map.set(transs.body.transaction, temp);
        }
      } else if (transs.type == 'delete') {
        if (transs.body != undefined) map.delete(transs.body.transaction);
      }
      a = transs._id;
      setLastId(a);
    });
    setcreated(Array.from(map, ([name, value]) => ({name, value})));
  };
  
  const createmsgs = () => {
    let tempObj;
    console.log(trans)
    created == [] ? (tempObj = {}) : (tempObj = {...created.map((val) => val.value)})
    trans.map((val) => {
      if (val.type === "new") {
        tempObj[val._id] = val;
      } else if (val.type === "edit") {
        tempObj[val.body.transaction].body = val.body;
      } else {
        delete tempObj[val.body.transaction];
      }
      setLastId(val._id)
    });
    setcreated(Object.values(tempObj).map((val) => { return { name: val._id, value: val } }));
  };

  const onLongPress = (user) => {
    const options = ['Edit Message', 'Delete Message', 'Cancel'];
    const cancelButtonIndex = options.length - 1;
    ActionSheet.showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex,
      },
      (buttonIndex) => {
        switch (buttonIndex) {
          case 0:
            setEdit(user.value.body.body);
            setModalVisible(!modalVisible);
            setEditConvo(user.value.conversation);
            setEditId(user.name);
            break;
          case 1:
            deleteMessage(user);
            break;
          case 2:
            break;
        }
      },
    );
  };

  const editMessage = () => {
    var config2 = {
      method: 'post',
      url: '/chat/transactions',
      headers: {
        Authorization: props.route.params.token,
        'Content-Type': 'application/json',
      },
      data: {
        type: 'edit',
        conversation: editConvo,
        transId: editId,
        body: edited,
      },
    };

    axios(config2)
      .then((res) => {
        createmsgs();
        setModalVisible(!modalVisible);
        Alert.alert('', 'Message Edited !');
      })
      .catch((err) => {
        Alert.alert('', 'Could not edit message !');
        console.log(err);
      });
  };

  const sendMessage = () => {
    var config2 = {
      method: 'post',
      url: '/chat/transactions',
      headers: {
        Authorization: props.route.params.token,
        'Content-Type': 'application/json',
      },
      data: {
        type: 'new',
        conversation: props.route.params.id,
        body: msg,
      },
    };

    axios(config2)
      .then((res) => {
        createmsgs();
        setMsg('');
        console.log('Message Added !');
      })
      .catch((err) => {
        console.log('Could not add message !');
        console.log(err);
      });
  };

  const deleteMessage = (chat) => {
    var config2 = {
      method: 'post',
      url: '/chat/transactions',
      headers: {
        Authorization: props.route.params.token,
        'Content-Type': 'application/json',
      },
      data: {
        type: 'delete',
        conversation: chat.value.conversation,
        transId: chat.name,
      },
    };

    axios(config2)
      .then((res) => {
        createmsgs();
        if (chat.name != -86) {
          Alert.alert('', 'Message Deleted !');
        }
      })
      .catch((err) => {
        if (chat.name != -86) {
          Alert.alert('', 'Could not delete message !');
          console.log(err);
        }
      });
  };

  // useEffect(() => {
  //   let temp = {
  //     value: {
  //       conversation: props.route.params.id,
  //     },
  //     name: -86,
  //   }
  //   deleteMessage(temp);
  // }, []) //Hack for seen status updates

  // useEffect(() => {
  //   var config = {
  //     method: 'get',
  //     url: '/chat/transactions',
  //     headers: {
  //       Authorization: props.route.params.token,
  //       'Content-Type': 'application/json',
  //     },
  //     params: {
  //       conversation: props.route.params.id,
  //       lastId: lastId,
  //     },
  //   };

  //   axios(config)
  //     .then((res) => {
  //       setTrans(res.data);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
    
  // }, []);
  
  useEffect(() => {
    var config = {
      method: 'get',
      url: '/chat/transactions',
      headers: {
        Authorization: props.route.params.token,
        'Content-Type': 'application/json',
      },
      params: {
        conversation: props.route.params.id,
        lastId: lastId,
      },
    };

    axios(config)
      .then((res) => {
        setTrans(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
    
  }, [lastId]);

  useEffect(() => {
    createmsgs();
  }, [trans]);

  useEffect(() => {
    if (props.route.params.name.indexOf(',') > -1) setIsGroup(true);
  }, []);

  useEffect(() => {
    if (props.route.params.name.indexOf(',') ===-1) {
      var config = {
        method: 'get',
        url: '/user/status',
        headers: {
          Authorization: props.route.params.token,
          'Content-Type': 'application/json',
        },
        params: {
          name: props.route.params.name.trim()
        },
      };

      axios(config)
        .then((res) => {
          setOnline(res.data[0].status);
          return res
        })
        .then((res) => {
          setUser(res);
          return res
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, []);

  useEffect(() => {
    AsyncStorage.getItem('userToken')
      .then((res) => {
        setSelf(res)
        return res
      })
      .then((res) => {
        socket.current = socketIO('http://021dff6c49d8.ngrok.io', {
          transports: ['websocket'],
        });
        socket.current.on('connect', () => {
          console.log('Connected finally?');
          console.log('======================================')
          console.log(socket.current.connected);
          socket.current.emit('login', JSON.parse(res).username);
          console.log('======================================')
        })
        socket.current.on('display', (data) => {
          if (!isTyping) {
            setWhoIsTyping(data.from);
            setIsTyping(true);
            setTimeout(() => {
              setIsTyping(false);
              setWhoIsTyping('');
            }, 1000);
          }
        })
        socket.current.on('online', (data) => {
          if (props.route.params.name.trim() == data) {
            setOnline(true);
          }
        })
        socket.current.on('offline', (data) => {
          if (props.route.params.name.trim() == data) {
            setOnline(false);
          }
        })
        socket.current.on('message', (data) => {
          if (data[0].username == JSON.parse(res).username) {
            setLastId(() => lastId - 1);
            // let temp = {
            //   value: {
            //     conversation: props.route.params.id,
            //   },
            //   name: -86,
            // }
            // deleteMessage(temp);
          }
        })
      })
      .catch((e) => {
        console.log(e);
      })

    return () => {
      socket.current.emit('logout');
    }
  }, []);

  const Item = ({chat}) => {return (
    <Text style={{
      marginBottom: 10,
      flex: 1,
      elevation: 5,
      alignSelf: props.route.params.name.indexOf(chat.value.from) === -1 ? 'flex-end' : 'flex-start',
      margin: 20,
    }}>

    <View>
      <TouchableWithoutFeedback onLongPress={() => {if (props.route.params.name.indexOf(chat.value.from) === -1) onLongPress(chat)}}>
        <View
          style={{
            backgroundColor: props.route.params.name.indexOf(chat.value.from) === -1 ? 'lightgreen' : 'lightgray',
            paddingTop: 10,
            paddingBottom: 5,
            borderTopEndRadius: 20,
            borderBottomLeftRadius: 20
          }}>
            <View style={{
              flexDirection: 'column', maxWidth: width - width/4,
              flexGrow: 1,
              flex: 1,}}>
            {isGroup && <Text style={{ fontSize: 10, marginRight: 20, marginLeft: 20, alignSelf: props.route.params.name.indexOf(chat.value.from) === -1 ? 'flex-end' : 'flex-start'}}>
              {chat.value.from}
            </Text>}
            <Text style={{ marginLeft: 20, marginRight: 20, fontSize: 15, fontWeight: 'bold', alignSelf: props.route.params.name.indexOf(chat.value.from) === -1 ? 'flex-end' : 'flex-start'}}>
              {chat.value.body.body}
            </Text>
              <Text style={{alignSelf: 'flex-end', marginRight: 5}}>
              <Text style={{
                fontSize: 10,
              }}>{moment(chat.value.body.seen ? chat.value.body.seen : chat.value.body.delivered ? chat.value.body.delivered : chat.value.body.sent).format('LT')} </Text>
                {props.route.params.name.indexOf(chat.value.from) === -1 && <Icon
                size={10}
                type="FontAwesome5"
                name={chat.value.body.seen ? 'check-circle' : chat.value.body.delivered ? 'check' : 'send'}
                color={chat.value.body.seen ? 'darkblue' : chat.value.body.delivered ? 'gray' : 'red'}
              />}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </View>
    </Text>
  )};

  const renderItem = ({item}) => <Item chat={item} />;
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{ flexDirection: 'row' }}>
        {!isGroup && <Icon style={{
          fontSize: 20,
          marginLeft: 20,
          marginTop: 30
        }}
          name='circle'
          color={online ? 'green' : 'gray'}
        />}
        <Text style={{
          fontSize: 20,
          marginLeft: 20,
          marginTop: 18
        }}>{props.route.params.name}</Text>
      </View>
      {isTyping && props.route.params.name.trim() == whoIsTyping ? <Text style={{
        fontSize: 12,
        marginLeft: 65,
        marginTop: -10
      }}>{isGroup ? 'typing...' : 'is typing...'}</Text> : online ? <Text style={{
        fontSize: 12,
        marginLeft: 65,
        marginTop: -10
        }}>online</Text> : isGroup ? <Text style={{
          fontSize: 12,
          marginLeft: 24,
          marginTop: 0
        }}>Group</Text> : <Text style={{
          fontSize: 12,
          marginLeft: 65,
          marginTop: -10
            }}>{user === '' ? 'Searching' : 'Last seen ' + moment(user.data[0].timestamp).subtract().calendar().toLowerCase()}</Text>}
      <Divider style={{ backgroundColor: 'blue', marginTop: 10 }} />
      <ScrollView ref={scrollViewRef}
        onContentSizeChange={(contentWidth, contentHeight) => { scrollViewRef.current.scrollToEnd({ animated: true }) }}>
        <Modal
          animationType="fade"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('', 'Modal has been closed.');
          }}>
          <View
            style={[
              styles.centeredView,
              modalVisible ? {backgroundColor: 'rgba(0,0,0,0.5)'} : '',
            ]}>
            <View style={styles.modalView}>
              <Text style={{color: 'black', fontSize: 20}}>Edit Message</Text>
              <Text />
              <TextInput
                placeholder="Enter New Message"
                value={edited}
                onChangeText={(text) => setEdit(text)}
                placeholderColor="#c4c3cb"
                style={styles.loginFormTextInput}
              />
              <View style={{flexDirection: 'row'}}>
                <TouchableHighlight
                  underlayColor="green"
                  style={{...styles.openButton, backgroundColor: '#357a38'}}
                  onPress={() => {
                    editMessage();
                    console.log('hi');
                  }}>
                  <Text style={styles.textStyle}>Edit</Text>
                </TouchableHighlight>
                <TouchableHighlight
                  underlayColor="red"
                  style={{
                    ...styles.openButton,
                    backgroundColor: '#b71c1c',
                    textAlign: 'right',
                    marginLeft: 20,
                  }}
                  onPress={() => {
                    setModalVisible(!modalVisible);
                  }}>
                  <Text style={styles.textStyle}>Close</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </Modal>
        {created.length === 0 ? <View style={{
          flex: 1,
          justifyContent: "center",
        }}><ActivityIndicator size="large" color="blue" /><Text style={{textAlign: 'center'}}>Getting your messages...</Text></View> : <FlatList
          data={created}
          renderItem={renderItem}
          keyExtractor={(item) => item.value._id.toString()}
        />}
      </ScrollView>
      <View>
        <KeyboardAvoidingView
          style={{
            position: 'relative',
            left: 0,
            right: 0,
            bottom: 0,
          }}
          behavior="position">
          <View style={{ flexDirection: 'row'}}>
            <TextInput
              placeholder="Type Something..."
              value={msg}
              onChangeText={(text) => {
                setMsg(text);
                if (props.route.params.name.indexOf(',') === -1 && self) {
                  socket.current.emit('typing', { to: props.route.params.name.trim(), from: JSON.parse(self).username });
                }
              }}
              placeholderColor="#c4c3cb"
              style={{...styles.loginFormTextInput, width: width-(width/3)}}
            />
            <TouchableHighlight
              underlayColor="green"
              style={{ ...styles.sendButton, backgroundColor: '#3e0e40'}}
              onPress={() => {
                sendMessage();
                console.log('hi');
              }}>
              <Text style={styles.sendStyle}><Icon
                name='sc-telegram'
                type='evilicon'
                color='white'
              /> Send</Text>
            </TouchableHighlight>
          </View>
        </KeyboardAvoidingView>
        </View>
    </SafeAreaView>
  );
};

export default ChatBox;
