import * as React from 'react';
import {
  View,
  Text,
  FlatList,
  Button,
  TouchableOpacity,
  TouchableHighlight,
  Alert,
  ActivityIndicator,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {AuthContext} from '../../utils/authContext';
import {Input, Card} from 'react-native-elements';
import axios from 'axios';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {ScrollView} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';

const Home = ({navigation}) => {
  const [token, setToken] = React.useState('');
  const [user, setUser] = React.useState('');
  const {signIn, signOut} = React.useContext(AuthContext);
  const [createConversation, setConversation] = React.useState('');
  const [SignUpErrors, setSignUpErrors] = React.useState({});
  const [chats, setChats] = React.useState([]);

  React.useEffect(() => {
    const bootstrapAsync = async () => {
      let userToken;
      try {
        userToken = await AsyncStorage.getItem('userToken');
        setUser(JSON.parse(userToken).username);
        setToken(JSON.parse(userToken).token);
        console.log(token);
      } catch (e) {
        console.log(e);
      }
    };
    bootstrapAsync();
    load();
  }, [token]);

  // const handleRecipientName = (recipients) => {
  //   let name = '';

  //   for (let i = 0; i < recipients.length; i++) {
  //     if (recipients[i] !== user) {
  //       name += recipients[i] + ', ';
  //     }
  //   }
  //   return name;
  // };

  const handleRecipientName = (recepients) => recepients.filter((name) => name !== user).join(', ')

  const createChat = () => {
    var config2 = {
      method: 'post',
      url: '/chat/create',
      headers: {
        Authorization: token,
        'Content-Type': 'application/json',
      },
      data: {
        members: createConversation,
      },
    };

    if (createConversation.length === 0) {
      Alert.alert('Please add atleast one username');
    } else {
      console.log(createConversation);
      axios(config2)
        .then((res) => {
          Alert.alert('', 'Conversation Added !');
          setConversation('');
        })
        .catch((err) => {
          Alert.alert('', 'Could not add Conversation !');
          console.log(err);
        });
    }
  };

  const load = () => {
    var config = {
      method: 'get',
      url: '/chat/conversations',
      headers: {
        Authorization: token,
        'Content-Type': 'application/json',
      },
    };
    axios(config)
      .then((res) => {
        setChats(res.data);
        console.log(res.data, 'chats');
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const Item = ({chat}) => (
    <TouchableOpacity
      onPress={() => {
        navigation.push('chatbox', {
          id: chat._id,
          name: handleRecipientName(chat.recipients),
          token: token,
        });
      }}>
      <Card style={{marginBottom: 10}}>
        <View style={{flexDirection: 'row'}}>
          <Icon name="account-circle" size={50} />
          <View style={{flexDirection: 'column'}}>
            <Text style={{fontSize: 18, marginLeft: 25, fontWeight: 'bold', marginBottom: 5}}>
              {handleRecipientName(chat.recipients)}
            </Text>
            <Text style={{marginLeft: 25, fontSize: 12}}>
              {chat.lastMessage === ' ' ? 'Hello World' : chats.lastMessage}
            </Text>
          </View>
        </View>
      </Card>
    </TouchableOpacity>
  );

  const renderItem = ({item}) => <Item chat={item} />;

  return (
    <ScrollView>
      <Button
        title="Sign out"
        color="#3e0e40"
        onPress={() => {
          signOut();
        }}
      />

      <ScrollView>
        <Card>
          <View style={{flex: 1}}>
            <Input
              placeholder="Create Conversation..."
              value={createConversation}
              onChangeText={setConversation}
              style={{flexDirection: 'row'}}
              errorStyle={{color: 'red'}}
              errorMessage={SignUpErrors ? SignUpErrors.convo : null}
            />
            <TouchableHighlight
              name="create"
              underlayColor="#2f3d60"
              onPress={() => createChat()}
              style={{
                backgroundColor: '#3e0e40',
                padding: 15,
                borderRadius: 10,
              }}>
              <Text style={{color: 'white', textAlign: 'center', fontSize: 20}}>
                <Icon name="add" /> Add Chat
              </Text>
            </TouchableHighlight>
          </View>
        </Card>
        {chats.length === 0 ? <View style={{
          flex: 1,
          justifyContent: "center",
        }}><ActivityIndicator size="large" color="blue" /><Text style={{ textAlign: 'center' }}>Getting your conversations...</Text></View> : 
        <FlatList
          data={chats}
          renderItem={renderItem}
          keyExtractor={(item) => item._id}
        />}
      </ScrollView>
    </ScrollView>
  );
};

export default Home;
