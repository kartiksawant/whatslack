// @flow
import 'react-native-gesture-handler';
import React, {useEffect, useContext, useMemo, useReducer} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Alert} from 'react-native';
import {MenuProvider} from 'react-native-popup-menu';
// Screens
import SignInScreen from './screens/auth/signInScreen';
import SignUpScreen from './screens/auth/signUpScreen';
import SplashScreen from './screens/splashScreen';
import Home from './screens/chat/Home';
import ChatBox from './screens/chat/chatbox';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {stateConditionString} from './utils/helpers';
import {AuthContext} from './utils/authContext';
import {reducer, initialState} from './reducer';

import axios from 'axios';
axios.defaults.baseURL = 'http://021dff6c49d8.ngrok.io';
const Stack = createStackNavigator();

const createHomeStack = () => {
  const {signOut} = useContext(AuthContext);

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        initialParams={{signOut: signOut}}
      />
      <Stack.Screen name="chatbox" component={ChatBox} />
    </Stack.Navigator>
  );
};

export default App = ({navigation}) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await AsyncStorage.getItem('userToken');
      } catch (e) {
        // Restoring token failed
      }
      dispatch({type: 'RESTORE_TOKEN', token: userToken});
    };
    bootstrapAsync();
  }, []);
  const authContextValue = useMemo(
    () => ({
      signIn: async (data) => {
        if (
          data &&
          data.emailAddress !== undefined &&
          data.password !== undefined
        ) {
          axios
            .post('/user/login', {
              username: data.emailAddress,
              password: data.password,
            })
            .then(function (response) {
              AsyncStorage.setItem('userToken', JSON.stringify(response.data));
              dispatch({type: 'SIGN_IN', token: JSON.stringify(response.data)});
            })
            .catch(function (error) {
              Alert.alert('Wrong credentials');
              dispatch({type: 'TO_SIGNIN_PAGE'});
              console.log(error);
            });
        } else {
          dispatch({type: 'TO_SIGNIN_PAGE'});
        }
      },
      signOut: async (data) => {
        console.log('signout');
        await AsyncStorage.removeItem('userToken');
        dispatch({type: 'SIGN_OUT'});
      },

      signUp: async (data) => {
        if (
          data &&
          data.emailAddress !== undefined &&
          data.password !== undefined
        ) {
          axios
            .post('/user/register', {
              username: data.emailAddress,
              password: data.password,
              password2: data.passwordConfirm,
            })
            .then(function (response) {
              Alert.alert('Registered succesfully');
              dispatch({type: 'TO_SIGNIN_PAGE'});
            })
            .catch(function (error) {
              Alert.alert('Error signing up');
              dispatch({type: 'TO_SIGNIN_PAGE'});
              console.log(error);
            });
        } else {
          dispatch({type: 'TO_SIGNUP_PAGE'});
        }
      },
    }),
    [],
  );

  const chooseScreen = (state) => {
    let navigateTo = stateConditionString(state);
    let arr = [];

    switch (navigateTo) {
      case 'LOAD_APP':
        arr.push(<Stack.Screen name="Splash" component={SplashScreen} />);
        break;

      case 'LOAD_SIGNUP':
        arr.push(
          <Stack.Screen
            name="SignUp"
            component={SignUpScreen}
            options={{
              title: 'Sign Up',
              animationTypeForReplace: state.isSignout ? 'pop' : 'push',
            }}
          />,
        );
        break;
      case 'LOAD_SIGNIN':
        arr.push(<Stack.Screen name="SignIn" component={SignInScreen} />);
        break;

      case 'LOAD_HOME':
        arr.push(
          <Stack.Screen
            name="Home"
            component={createHomeStack}
            options={{
              title: 'WhatSlack',
              headerStyle: {backgroundColor: '#3e0e40'},
              headerTintColor: 'white',
            }}
          />,
        );
        break;
      default:
        arr.push(<Stack.Screen name="SignIn" component={SignInScreen} />);
        break;
    }
    return arr[0];
  };

  return (
    <AuthContext.Provider value={authContextValue}>
      <MenuProvider>
        <NavigationContainer>
          <Stack.Navigator>{chooseScreen(state)}</Stack.Navigator>
        </NavigationContainer>
      </MenuProvider>
    </AuthContext.Provider>
  );
};
