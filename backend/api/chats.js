const express = require("express");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const router = express.Router();

const verify = require("../utilities/verify-token");
const Transaction = require("../models/Transaction");
const Conversation = require("../models/Conversation");
const User = require("../models/User");

let jwtUser = null;

// Token verfication middleware
router.use(function (req, res, next) {
  try {
    jwtUser = jwt.verify(verify(req), process.env.AUTHSECRET);
    next();
  } catch (err) {
    console.log(err);
    res.setHeader("Content-Type", "application/json");
    res.status(401).send(JSON.stringify({ message: "Unauthorized" }));
  }
});
//Create a conversation
router.post("/create", async (req, res) => {
  let from = mongoose.Types.ObjectId(jwtUser.id);
  let members = req.body.members.split(" ").join("").split(",");
  let recipients = [];
  try {
    const username = await User.find({ _id: from }).exec();
    members.push(username[0].username);
    async function checkUsers() {
      for (const member of members) {
        const contents = await User.find({ username: member }).exec();
        if (contents.length != 0) {
          recipients.push(member);
        }
      }
    }
    const data = await checkUsers();
    if (recipients.length == 1) {
      res.status(400).send("Users do not exist");
    } else {
      console.log(recipients);
      const convo = new Conversation({
        recipients: recipients,
        lastMessage: " ",
        date: Date.now(),
      });
      convo.save(function (err) {
        if (err) {
          res.status(500).send(err);
        } else {
          recipients.forEach((users) => {
            User.find({ username: users })
              .then((soc) => {
                socketvalue = soc[0].socket;
                req.io.to(socketvalue).emit("message", soc);
              })
              .catch((err) => {
                console.log("Cant get socket");
              });
          });
          res.status(201).send("success");
        }
      });
    }
  } catch (error) {
    res.status(400).send(error);
  }
});

// Get conversations list
router.get("/conversations", (req, res) => {
  let from = mongoose.Types.ObjectId(jwtUser.id);
  User.find({ _id: from })
    .then((data) => {
      let id = data[0].username;
      Conversation.find({ recipients: id })
        .then((data) => {
          // console.log(data, id);
          data.forEach((user) => {
            Transaction.find({
              from: { $ne: id },
              conversation: user._id,
              "body.delivered": 0,
              type: "new",
            })
              .then((data) => {
                //console.log(data);
                data.forEach((del) => {
                  console.log(del._id, "got it");
                  Transaction.updateOne(
                    { _id: del._id },
                    {
                      $set: { "body.delivered": Date.now() },
                    },
                    () => {
                      console.log("updated");
                    }
                  );
                });
                // console.log(data);
              })
              .catch((err) => {
                res / status(400).send(err);
              });
          });

          res.status(200).send(data);
        })
        .catch((err) => {
          res.status(400).send(err);
        });
    })
    .catch((err) => {
      res.status(400).send(err);
    });
});

// Get transactions
router.get("/transactions", (req, res) => {
  let from = mongoose.Types.ObjectId(jwtUser.id);

  User.find({ _id: from })
    .then((data) => {
      let id = data[0].username;
      Conversation.find({ _id: req.query.conversation }).then((data) => {
        data[0].recipients.forEach((user) => {
          if (user != id) {
            Transaction.find({
              from: user,
              conversation: req.query.conversation,
              "body.seen": 0,
              type: "new",
            }).then((data) => {
              data.forEach((del) => {
                console.log(del._id, "yooo");
                Transaction.updateOne(
                  { _id: del._id },
                  {
                    $set: { "body.seen": Date.now() },
                  },
                  () => {
                    console.log("updated");
                  }
                );
              });
            });
          }
        });
      });
      Transaction.find({
        conversation: req.query.conversation,
        _id: { $gt: req.query.lastId },
      })
        .then((data) => {
          res.send(data);
        })
        .catch((err) => {
          res.status(400).send(err);
        });
    })
    .catch((err) => {
      res.status(400).send(err);
    });
});

// Post transaction
router.post("/transactions", (req, res) => {
  let delDate = null;
  let from = mongoose.Types.ObjectId(jwtUser.id);
  User.find({ _id: from })
    .then((data) => {
      let id = data[0].username;
      if (req.body.type == "delete" && req.body.transId != -86) {
        const transaction = new Transaction({
          type: req.body.type,
          from: id,
          conversation: req.body.conversation,
          body: {
            transaction: req.body.transId,
          },
        });
        transaction.save(function (err) {
          if (err) {
            res.status(500).send(err);
          } else {
            res.status(201).send("success");
          }
        });
      } else if (req.body.type == "new") {
        const transaction = new Transaction({
          type: req.body.type,
          from: id,
          conversation: req.body.conversation,
          body: {
            body: req.body.body,
            sent: Date.now(),
            delivered: 0,
            seen: 0,
          },
        });

        // Conversation.find({ _id: req.body.conversation })
        //   .then((data) => {
        //     data[0].recipients.forEach((user) => {
        //       User.find({ username: user })
        //         .then((data) => {
        //           if (data[0].status == true) {
        //             console.log(transaction.body.delivered, data[0].status);
        //             transaction.body.delivered = null;
        //           }
        //         })
        //         .catch((err) => {
        //           res.status(400).send(err);
        //         });
        //     });
        //     console.log(transaction.body.delivered, "yes");
        //   })
        //   .catch((err) => {
        //     res.status(400).send(err);
        //   });


        transaction.save(function (err) {
          if (err) {
            res.status(500).send(err);
          } else {
            res.status(201).send("success");
          }
        });
      } else if (req.body.type == "edit") {
        const transaction = new Transaction({
          type: req.body.type,
          from: id,
          conversation: req.body.conversation,
          body: {
            body: req.body.body,
            transaction: req.body.transId,
          },
        });
        transaction.save(function (err) {
          if (err) {
            res.status(500).send(err);
          } else {
            res.status(201).send("success");
          }
        });
      }
      Conversation.find({ _id: req.body.conversation }).then((data) => {
        data[0].recipients.forEach((receiver) => {
          User.find({ username: receiver })
            .then((soc) => {
              socketvalue = soc[0].socket;
              req.io.to(socketvalue).emit("message", soc);
            })
            .catch((err) => {
              console.log("Cant get socket");
            });
        });
      });
    })
    .catch((err) => {
      res.status(400).send(err);
    });
});
module.exports = router;
