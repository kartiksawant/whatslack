const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const cors = require("cors");
const dotenv = require("dotenv");
const User = require("./models/User");
const users = require("./api/users");
const chats = require("./api/chats");

const app = express();

const port = process.env.PORT || 5000;
const server = app.listen(port, () =>
  console.log(`Server running on port ${port}`)
);

const io = require("socket.io").listen(server);
// CORS middleware
app.use(cors());

// Environment initialization
dotenv.config();

// Body Parser middleware to parse request bodies
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(bodyParser.json());


// Database configuration
const db = process.env.MONGOURI;

mongoose
  .connect(db, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => console.log("MongoDB Successfully Connected"))
  .catch((err) => console.log(err));

// Passport middleware
app.use(passport.initialize());
// Passport config
require("./config/passport")(passport);

const updateSocket = (user, id) => {
  console.log(`Update socket ${id} for ${user}`)
  User.updateOne({ username: user }, { $set: { socket: id } }, () => {
    console.log("Socket updated");
  });
};

const userOffline = (user) => {
  //Update offline status
  User.updateOne(
    { username: user },
    { $set: { status: false, timestamp: Date.now() } },
    () => {
      console.log("User offline");
    }
  );
};

let status = false,
  socketvalue = "";

const getSocket = (user) => {
  User.find({ username: user })
    .then((data) => {
      status = data[0].status;
      socketvalue = data[0].socket;
    })
    .catch((err) => {
      console.log("Cant get socket");
    });
};

const userOnline = (user) => {
  //Update online status
  User.updateOne({ username: user }, { $set: { status: true } }, () => {
    console.log(user + " online");
  });
};

io.on("connect", (socket) => {
  socket.on("login", (data) => {
    console.log('================')
    console.log(data)
    console.log('================')
    socket.username = data;
    updateSocket(data, socket.id);
    userOnline(data);
    socket.broadcast.emit("online", data);
  });

  socket.on("typing", function (data) {
    console.log(data.from, "typing to", data.to);
    getSocket(data.to);
    console.log(socketvalue, status);
    if (status == true)
      io.to(socketvalue).emit("display", { status: true, from: data.from });
  });

  socket.on("typingend", function (data) {
    console.log(socket.username);
    getSocket(data.to);
    if (status == true)
      io.to(socketvalue).emit("display", { status: false, from: data.from });
  });

  socket.on("logout", () => {
    console.log(socket.username + " disconnected");
    socket.broadcast.emit("offline", socket.username);
    userOffline(socket.username);
    status = false;
    socketvalue = "";
  });

  socket.on("disconnect", () => {
    console.log(socket.username + " disconnected");
    socket.broadcast.emit("offline", socket.username);
    userOffline(socket.username);
    status = false;
    socketvalue = "";
  });
});

// Assign socket object to every request
app.use(function (req, res, next) {
  req.io = io;
  next();
});

// Routes
app.use("/user", users);
app.use("/chat", chats);
