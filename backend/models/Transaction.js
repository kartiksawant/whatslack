const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement= require('mongoose-auto-increment');
// Create Schema for Users
const TransactionSchema = new Schema({
    conversation: {
        type: Schema.Types.ObjectId,
        ref: 'conversations',
    },
    type: {
        type: String,
        required: true,
    },
    from: {
        type: String,
        ref: 'users',
    },
    body: {
        type: Object,
        required: true,
    },
});
autoIncrement.initialize(mongoose.connection);
TransactionSchema.plugin(autoIncrement.plugin, 'transactions');
module.exports = Transaction = mongoose.model('transactions', TransactionSchema);