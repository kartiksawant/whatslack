const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema for Users
const UserSchema = new Schema({
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  status: {
    type: Boolean,
    required: true,
    default: false,
  },
  socket: {
    type: String,
    default: "",
  },
  timestamp: {
    type: Date,
    required: true,
    default: Date.now,
  },
});

const User = mongoose.model("User", UserSchema);

module.exports = User;
